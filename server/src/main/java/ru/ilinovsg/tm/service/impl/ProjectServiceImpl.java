package ru.ilinovsg.tm.service.impl;

import ru.ilinovsg.tm.dto.ProjectDTO;
import ru.ilinovsg.tm.dto.ProjectListResponseDTO;
import ru.ilinovsg.tm.dto.ProjectResponseDTO;
import ru.ilinovsg.tm.enumerated.Status;
import ru.ilinovsg.tm.mapper.ProjectMapper;
import ru.ilinovsg.tm.repository.ProjectRepository;
import ru.ilinovsg.tm.service.ProjectService;
import ru.ilinovsg.tm.entity.Project;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ProjectServiceImpl implements ProjectService {
    private static ProjectServiceImpl instance = null;
    private final ProjectRepository projectRepository;

    private ProjectServiceImpl() {
        projectRepository = null;
    }

    private ProjectServiceImpl(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public static ProjectServiceImpl getInstance(ProjectRepository projectRepository) {
        if (instance == null) {
            synchronized (ProjectServiceImpl.class) {
                if (instance == null) {
                    instance = new ProjectServiceImpl(projectRepository);
                }
            }
        }
        return instance;
    }

    @Override
    public ProjectResponseDTO createProject(ProjectDTO projectDTO) {
        Project project = Project.builder()
                .name(projectDTO.getName())
                .description(projectDTO.getDescription())
                .userId(projectDTO.getUserId())
                .build();
        Optional<Project> projectOptional = projectRepository.create(project);
        if (projectOptional.isPresent()) {
            return ProjectResponseDTO.builder().payload(ProjectMapper.toDto(projectOptional.get())).status(Status.OK).build();
        }
        return ProjectResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ProjectResponseDTO updateProject(ProjectDTO projectDTO) {
        Project project = Project.builder()
                .id(projectDTO.getId())
                .name(projectDTO.getName())
                .description(projectDTO.getDescription())
                .userId(projectDTO.getUserId())
                .build();
        Optional<Project> projectOptional = projectRepository.update(project);
        if (projectOptional.isPresent()) {
            return ProjectResponseDTO.builder().payload(ProjectMapper.toDto(projectOptional.get())).status(Status.OK).build();
        }
        return ProjectResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ProjectResponseDTO deleteProject(Long id) {
        projectRepository.delete(id);
        return ProjectResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ProjectResponseDTO getProject(Long id) {
        Optional<Project> projectOptional = projectRepository.findById(id);
        if (projectOptional.isPresent()) {
            return ProjectResponseDTO.builder().payload(ProjectMapper.toDto(projectOptional.get())).status(Status.OK).build();
        }
        return ProjectResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ProjectListResponseDTO getAllProjects() {
        List<Project> projects = projectRepository.findAll();
        ProjectDTO[] projectArray = projects.stream().map(project -> ProjectMapper.toDto(project)).toArray(ProjectDTO[]::new);
        return ProjectListResponseDTO.builder().status(Status.OK).payload(projectArray).build();
    }

    @Override
    public ProjectListResponseDTO findByName(String name) {
        List<Project> projects = projectRepository.findAll().stream().filter(
                project -> name.equals(project.getName()))
                .collect(Collectors.toList());
        ProjectDTO[] projectArray = projects.stream().map(project -> ProjectMapper.toDto(project)).toArray(ProjectDTO[]::new);
        return ProjectListResponseDTO.builder().status(Status.OK).payload(projectArray).build();
    }
}

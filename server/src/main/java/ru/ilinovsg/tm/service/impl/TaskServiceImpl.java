package ru.ilinovsg.tm.service.impl;

import ru.ilinovsg.tm.dto.TaskDTO;
import ru.ilinovsg.tm.dto.TaskListResponseDTO;
import ru.ilinovsg.tm.dto.TaskResponseDTO;
import ru.ilinovsg.tm.enumerated.Status;
import ru.ilinovsg.tm.mapper.TaskMapper;
import ru.ilinovsg.tm.repository.TaskRepository;
import ru.ilinovsg.tm.entity.Task;
import ru.ilinovsg.tm.service.TaskService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TaskServiceImpl implements TaskService {
    private static TaskServiceImpl instance = null;
    private final TaskRepository taskRepository;

    private TaskServiceImpl() {
        taskRepository = null;
    }

    private TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public static TaskServiceImpl getInstance(TaskRepository taskRepository) {
        if (instance == null) {
            synchronized (TaskServiceImpl.class) {
                if (instance == null) {
                    instance = new TaskServiceImpl(taskRepository);
                }
            }
        }
        return instance;
    }

    @Override
    public TaskResponseDTO createTask(TaskDTO taskDTO) {
        Task task = Task.builder()
                .name(taskDTO.getName())
                .description(taskDTO.getDescription())
                .userId(taskDTO.getUserId())
                .projectId(taskDTO.getProjectId())
                .build();
        Optional<Task> taskOptional = taskRepository.create(task);
        if (taskOptional.isPresent()) {
            return TaskResponseDTO.builder().payload(TaskMapper.toDto(taskOptional.get())).status(Status.OK).build();
        }
        return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public TaskResponseDTO updateTask(TaskDTO taskDTO) {
        Task task = Task.builder()
                .id(taskDTO.getId())
                .name(taskDTO.getName())
                .description(taskDTO.getDescription())
                .userId(taskDTO.getUserId())
                .projectId(taskDTO.getProjectId())
                .build();
        Optional<Task> taskOptional = taskRepository.update(task);
        if (taskOptional.isPresent()) {
            return TaskResponseDTO.builder().payload(TaskMapper.toDto(taskOptional.get())).status(Status.OK).build();
        }
        return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public TaskResponseDTO deleteTask(Long id) {
            taskRepository.delete(id);
            return TaskResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public TaskResponseDTO getTask(Long id) {
        Optional<Task> taskOptional = taskRepository.findById(id);
        if (taskOptional.isPresent()) {
            return TaskResponseDTO.builder().payload(TaskMapper.toDto(taskOptional.get())).status(Status.OK).build();
        }
        return TaskResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public TaskListResponseDTO getAllTasks() {
        List<Task> tasks = taskRepository.findAll();
        TaskDTO[] taskArray = tasks.stream().map(task -> TaskMapper.toDto(task)).toArray(TaskDTO[]::new);
        return TaskListResponseDTO.builder().status(Status.OK).payload(taskArray).build();
    }

    @Override
    public TaskListResponseDTO findByName(String name) {
        List<Task> tasks = taskRepository.findAll().stream().filter(
                task -> name.equals(task.getName()))
                .collect(Collectors.toList());
        TaskDTO[] taskArray = tasks.stream().map(task -> TaskMapper.toDto(task)).toArray(TaskDTO[]::new);
        return TaskListResponseDTO.builder().status(Status.OK).payload(taskArray).build();
    }
}

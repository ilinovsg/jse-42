package ru.ilinovsg.tm.controller.impl;

import lombok.extern.log4j.Log4j2;
import ru.ilinovsg.tm.controller.TaskController;
import ru.ilinovsg.tm.dto.TaskDTO;
import ru.ilinovsg.tm.dto.TaskListResponseDTO;
import ru.ilinovsg.tm.dto.TaskResponseDTO;
import ru.ilinovsg.tm.service.TaskService;

import javax.jws.WebService;

@Log4j2
@WebService(endpointInterface = "ru.ilinovsg.tm.controller.TaskController")
public class TaskControllerImpl implements TaskController {
    private TaskService taskService;

    public TaskControllerImpl() {
    }

    public void setTaskService(TaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public TaskResponseDTO createTask(TaskDTO taskDTO) {
        return taskService.createTask(taskDTO);
    }

    @Override
    public TaskResponseDTO updateTask(TaskDTO taskDTO) {
        return taskService.updateTask(taskDTO);
    }

    @Override
    public TaskResponseDTO deleteTask(Long id) {
        return taskService.deleteTask(id);
    }

    @Override
    public TaskResponseDTO getTask(Long id) {
        return taskService.getTask(id);
    }

    @Override
    public TaskListResponseDTO getAllTasks() {
        return taskService.getAllTasks();
    }

    @Override
    public TaskListResponseDTO findByName(String name) {
        return taskService.findByName(name);
    }
}

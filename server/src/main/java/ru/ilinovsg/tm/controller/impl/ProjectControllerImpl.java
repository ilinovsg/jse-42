package ru.ilinovsg.tm.controller.impl;

import lombok.extern.log4j.Log4j2;
import ru.ilinovsg.tm.service.ProjectService;
import ru.ilinovsg.tm.controller.ProjectController;
import ru.ilinovsg.tm.dto.ProjectDTO;
import ru.ilinovsg.tm.dto.ProjectListResponseDTO;
import ru.ilinovsg.tm.dto.ProjectResponseDTO;

import javax.jws.WebService;

@Log4j2
@WebService(endpointInterface = "ru.ilinovsg.tm.controller.ProjectController")
public class ProjectControllerImpl implements ProjectController {
    private ProjectService projectService;

    public ProjectControllerImpl() {
    }

    public void setProjectService(ProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public ProjectResponseDTO createProject(ProjectDTO projectDTO) {
        return projectService.createProject(projectDTO);
    }

    @Override
    public ProjectResponseDTO updateProject(ProjectDTO projectDTO) {
        return projectService.updateProject(projectDTO);
    }

    @Override
    public ProjectResponseDTO deleteProject(Long id) {
        return projectService.deleteProject(id);
    }

    @Override
    public ProjectResponseDTO getProject(Long id) {
        return projectService.getProject(id);
    }

    @Override
    public ProjectListResponseDTO getAllProjects() {
        return projectService.getAllProjects();
    }

    @Override
    public ProjectListResponseDTO findByName(String name) {
        return projectService.findByName(name);
    }
}

package ru.ilinovsg.tm.controller;


import ru.ilinovsg.tm.dto.TaskDTO;
import ru.ilinovsg.tm.dto.TaskListResponseDTO;
import ru.ilinovsg.tm.dto.TaskResponseDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface TaskController {
    @WebMethod
    TaskResponseDTO createTask(TaskDTO taskDTO);

    @WebMethod
    TaskResponseDTO updateTask(TaskDTO taskDTO);

    @WebMethod
    TaskResponseDTO deleteTask(Long id);

    @WebMethod
    TaskResponseDTO getTask(Long id);

    @WebMethod
    TaskListResponseDTO getAllTasks();

    @WebMethod
    TaskListResponseDTO findByName(String name);
}


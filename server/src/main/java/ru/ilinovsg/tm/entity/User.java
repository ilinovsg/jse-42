package ru.ilinovsg.tm.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.ilinovsg.tm.annotation.MapTo;
import ru.ilinovsg.tm.enumerated.Role;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User extends AbstractEntity{
    @MapTo
    private Long id;
    @MapTo
    private String login;
    @MapTo
    private String password;
    @MapTo
    private String firstName;
    @MapTo
    private String lastName;
}

package ru.ilinovsg.tm.repository;

import ru.ilinovsg.tm.entity.AbstractEntity;

import java.util.*;

public class AbstractRepository<E extends AbstractEntity> implements Repository<E>{
    private final Map<Long, E> storage = new HashMap<>();

    @Override
    public Optional<E> create(E value) {
        synchronized (storage) {
            Long id = findNewKey();
            value.setId(id);
            storage.put(id, value);
        }
        return Optional.ofNullable(value);
    }

    private Long findNewKey() {
        if (storage.size() > 0) {
            return Collections.max(storage.keySet()) + 1L;
        }
        return 1L;
    }

    @Override
    public Optional<E> update(E value) {
        storage.put(value.getId(), value);
        return Optional.of(value);
    }

    @Override
    public Optional<E> findById(Long id) {
        return Optional.ofNullable(storage.get(id));
    }

    @Override
    public void delete(Long id) {
        storage.remove(id);
    }

    @Override
    public List<E> findAll() {
        return new ArrayList<>(storage.values());
    }
}

package ru.ilinovsg.tm.repository.impl.mapper;

import ru.ilinovsg.tm.entity.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper {
    public static User getUserFromResultSet(ResultSet resultSet) throws SQLException {
        return User.builder()
                .id(resultSet.getLong("id"))
                .login(resultSet.getString("login"))
                .password(resultSet.getString("password"))
                .firstName(resultSet.getString("firstname"))
                .lastName(resultSet.getString("lastname"))
                .build();
    }

    public static void setupStatement(PreparedStatement preparedStatement, User input) throws SQLException {
        preparedStatement.setString(1, input.getLogin());
        preparedStatement.setString(2, input.getPassword());
        preparedStatement.setString(3, input.getFirstName());
        preparedStatement.setString(4, input.getLastName());
    }
}

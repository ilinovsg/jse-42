package ru.ilinovsg.tm.repository;

import ru.ilinovsg.tm.entity.Project;

public interface ProjectRepository extends Repository<Project>{
}

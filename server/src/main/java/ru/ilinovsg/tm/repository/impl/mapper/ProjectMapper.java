package ru.ilinovsg.tm.repository.impl.mapper;

import ru.ilinovsg.tm.entity.Project;
import ru.ilinovsg.tm.entity.Task;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProjectMapper {
    public static Project getProjectFromResultSet(ResultSet resultSet) throws SQLException {
        return Project.builder()
                .id(resultSet.getLong("id"))
                .name(resultSet.getString("name"))
                .description(resultSet.getString("description"))
                .userId(resultSet.getLong("userId"))
                .build();
    }

    public static void setupStatement(PreparedStatement preparedStatement, Project input) throws SQLException {
        preparedStatement.setString(1, input.getName());
        preparedStatement.setString(2, input.getDescription());
        preparedStatement.setLong(3, input.getUserId());
    }
}
